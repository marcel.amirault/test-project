---
comments: false
description: 'Learn how to use and administer GitLab, the most scalable Git-based fully integrated platform for software development.'
---

<div class="display-none">
  <em>Visit <a href="https://docs.gitlab.com/ee/">docs.gitlab.com</a> for optimized
  navigation, discoverability, and readability.</em>
</div>
<!-- the div above will not display on the docs site asdf asdf but will display on /help -->

# GitLab Docs

Welcome to [GitLab](https://about.gitlab.com/) documentation.

Here you can access the complete documentation for GitLab, the single application for the
[entire DevOps lifecycle](#the-entire-devops-lifecycle).

## Overview

No matter how you use GitLab, we have documentation for you.

| Essential documentation                                                                                                              | Essential documentation                                                                                                    |
|:-------------------------------------------------------------------------------------------------------------------------------------|:---------------------------------------------------------------------------------------------------------------------------|
| [**User Documentation**](user/index.md)<br/>Discover features and concepts for GitLab users.                                         | [**Administrator documentation**](administration/index.md)<br/>Everything GitLab self-managed administrators need to know. |
| [**Contributing to GitLab**](#contributing-to-gitlab)<br/>At GitLab, everyone can contribute!                                        | [**New to Git and GitLab?**](#new-to-git-and-gitlab)<br/>We have the resources to get you started.                         |
| [**Build an integration with GitLab?**](#build-an-integration-with-gitlab)<br/>Consult our automation and integration documentation. | [**Coming to GitLab from another platform?**](#coming-to-gitlab-from-another-platform)<br/>Consult our handy guides.       |
| [**Install GitLab**](https://about.gitlab.com/install/)<br/>Installation options for different platforms.                            | [**Customers**](subscriptions/index.md)<br/>Information for new and existing customers.                                    |
| [**Update GitLab**](update/README.md)<br/>Update your GitLab self-managed instance to the latest version.                            | [**Reference Architectures**](administration/reference_architectures/index.md)<br/>GitLab's reference architectures        |
| [**GitLab Releases**](https://about.gitlab.com/releases/)<br/>What's new in GitLab.                                                  |                                                                                                                            |

## Popular topics

Have a look at some of our most popular topics:

| Popular topic                                                                                              | Description                                                                |
|:-----------------------------------------------------------------------------------------------------------|:---------------------------------------------------------------------------|
| [Two-factor authentication](user/profile/account/two_factor_authentication.md)                             | Improve the security of your GitLab account.                               |
| [GitLab groups](user/group/index.md)                                                                       | Manage projects together.                                                  |
| [GitLab CI/CD pipeline configuration reference](ci/yaml/README.md)                                         | Available configuration options for `.gitlab-ci.yml` files.                |
| [Activate GitLab EE with a license](user/admin_area/license.md) **(STARTER ONLY)**                         | Activate GitLab Enterprise Edition functionality with a license.           |
| [Back up and restore GitLab](raketasks/backup_restore.md) **(CORE ONLY)**                                  | Rake tasks for backing up and restoring GitLab self-managed instances.     |
| [GitLab release and maintenance policy](policy/maintenance.md)                                             | Policies for version naming and cadence, and also upgrade recommendations. |
| [Elasticsearch integration](integration/elasticsearch.md) **(STARTER ONLY)**                               | Integrate Elasticsearch with GitLab to enable advanced searching.          |
| [Omnibus GitLab database settings](https://docs.gitlab.com/omnibus/settings/database.html) **(CORE ONLY)** | Database settings for Omnibus GitLab self-managed instances.               |
| [Omnibus GitLab NGINX settings](https://docs.gitlab.com/omnibus/settings/nginx.html) **(CORE ONLY)**       | NGINX settings for Omnibus GitLab self-managed instances.                  |
| [Omnibus GitLab SSL configuration](https://docs.gitlab.com/omnibus/settings/ssl.html) **(CORE ONLY)**      | SSL settings for Omnibus GitLab self-managed instances.                    |
| [GitLab.com settings](user/gitlab_com/index.md)                                                            | Settings used for GitLab.com.                                              |

## The entire DevOps Lifecycle

GitLab is the first single application for software development, security,
and operations that enables [Concurrent DevOps](https://about.gitlab.com/topics/concurrent-devops/),
making the software lifecycle faster and radically improving the speed of business.

GitLab provides solutions for [each of the stages of the DevOps lifecycle](https://about.gitlab.com/stages-devops-lifecycle/):

![DevOps Stages](img/devops-stages-13_3.png)

GitLab is like a top-of-the-line kitchen for making software. As the executive
chef, you decide what software you want to serve. Using your recipe, GitLab handles
all the prep work, cooking, and delivery, so you can turn around orders faster
than ever.

The following sections provide links to documentation for each DevOps stage:

| DevOps stage            | Documentation for                                           |
|:------------------------|:------------------------------------------------------------|
| [Manage](#manage)       | Statistics and analytics features.                          |
| [Plan](#plan)           | Project planning and management features.                   |
| [Create](#create)       | Source code, data creation, and management features.        |
| [Verify](#verify)       | Testing, code quality, and continuous integration features. |
| [Package](#package)     | Docker container registry.                                  |
| [Secure](#secure)       | Security capability features.                               |
| [Release](#release)     | Application release and delivery features.                  |
| [Configure](#configure) | Application and infrastructure configuration tools.         |
| [Monitor](#monitor)     | Application monitoring and metrics features.                |
| [Defend](#defend)       | Protection against security intrusions.                     |

<div align="right">
  <a type="button" class="btn btn-default" href="#overview">
    Back to Overview <i class="fa fa-angle-double-up" aria-hidden="true"></i>
  </a>
</div>

### Manage

GitLab provides statistics and insight into ways you can maximize the value of GitLab in your organization.

The following documentation relates to the DevOps **Manage** stage:

| Manage topics                                                                         | Description                                                                                                                                                                                                                  |
|:--------------------------------------------------------------------------------------|:-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| [Authentication and<br/>Authorization](administration/auth/README.md) **(CORE ONLY)** | Supported authentication and authorization providers.                                                                                                                                                                        |
| [GitLab Value Stream Analytics](user/project/cycle_analytics.md)                      | Measure the time it takes to go from an [idea to production](https://about.gitlab.com/blog/2016/08/05/continuous-integration-delivery-and-deployment-with-gitlab/#from-idea-to-production-with-gitlab) for each project you have. |
| [Instance-level Analytics](user/admin_area/analytics/index.md)                              | Discover statistics on how many GitLab features you use and user activity.                                                                                                                                                   |

<div align="right">
  <a type="button" class="btn btn-default" href="#overview">
    Back to Overview <i class="fa fa-angle-double-up" aria-hidden="true"></i>
  </a>
</div>

### Plan

Whether you use Waterfall, Agile, or Conversational Development, GitLab streamlines your collaborative workflows.

Visualize, prioritize, coordinate, and track your progress your way with GitLab’s flexible project
management tools.

The following documentation relates to the DevOps **Plan** stage:

| Plan topics                                                                                                                                                                                                                                                        | Description                                                                                                                                      |
|:-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|:-------------------------------------------------------------------------------------------------------------------------------------------------|
| [Burndown Charts](user/project/milestones/burndown_charts.md) **(STARTER)**                                                                                                                                                                                        | Watch your project's progress throughout a specific milestone.                                                                                   |
| [Discussions](user/discussions/index.md)                                                                                                                                                                                                                           | Threads, comments, and resolvable threads in issues, commits, and merge requests.                                                           |
| [Due Dates](user/project/issues/due_dates.md)                                                                                                                                                                                                                      | Keep track of issue deadlines.                                                                                                                   |
| [Epics](user/group/epics/index.md) **(ULTIMATE)**                                                                                                                                                                                                                  | Tracking groups of issues that share a theme.                                                                                                    |
| [Issues](user/project/issues/index.md), including [confidential issues](user/project/issues/confidential_issues.md),<br/>[issue and merge request templates](user/project/description_templates.md),<br/>and [moving issues](user/project/issues/managing_issues.md#moving-issues) | Project issues and restricting access to issues as well as creating templates for submitting new issues and merge requests. Also, moving issues between projects. |
| [Labels](user/project/labels.md)                                                                                                                                                                                                                                   | Categorize issues or merge requests with descriptive labels.                                                                                     |
| [Milestones](user/project/milestones/index.md)                                                                                                                                                                                                                     | Set milestones for delivery of issues and merge requests, with optional due date.                                                                |
| [Project Issue Board](user/project/issue_board.md)                                                                                                                                                                                                                 | Display issues on a Scrum or Kanban board.                                                                                                       |
| [Quick Actions](user/project/quick_actions.md)                                                                                                                                                                                                                     | Shortcuts for common actions on issues or merge requests, replacing the need to click buttons or use dropdowns in GitLab's UI.                   |
| [Related Issues](user/project/issues/related_issues.md) **(STARTER)**                                                                                                                                                                                              | Create a relationship between issues.                                                                                                            |
| [Requirements Management](user/project/requirements/index.md) **(ULTIMATE)** | Check your products against a set of criteria. |
| [Roadmap](user/group/roadmap/index.md) **(ULTIMATE)**                                                                                                                                                                                                              | Visualize epic timelines.                                                                                                                        |
| [Service Desk](user/project/service_desk.md)                                                                                                                                                                                                         | A simple way to allow people to create issues in your GitLab instance without needing their own user account.                                    |
| [Time Tracking](user/project/time_tracking.md)                                                                                                                                                                                                                         | Track time spent on issues and merge requests.                                                                                                   |
| [Todos](user/todos.md)                                                                                                                                                                                                                                         | Keep track of work requiring attention with a chronological list displayed on a simple dashboard.                                                |

<div align="right">
  <a type="button" class="btn btn-default" href="#overview">
    Back to Overview <i class="fa fa-angle-double-up" aria-hidden="true"></i>
  </a>
</div>

### Create

Consolidate source code into a single [distributed version control system](https://en.wikipedia.org/wiki/Distributed_version_control)
that’s easily managed and controlled without disrupting your workflow.

GitLab repositories come complete with branching tools and access
controls, providing a scalable, single source of truth for collaborating
on projects and code.

The following documentation relates to the DevOps **Create** stage:

#### Projects and Groups

| Create topics - Projects and Groups                                                                                                                                                              | Description                                                                                      |
|:-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|:-------------------------------------------------------------------------------------------------|
| [Advanced search](user/search/advanced_global_search.md) **(STARTER)**                                                                                                                    | Leverage Elasticsearch for faster, more advanced code search across your entire GitLab instance. |
| [Advanced syntax search](user/search/advanced_search_syntax.md) **(STARTER)**                                                                                                                    | Use advanced queries for more targeted search results.                                           |
| [Contribution analytics](user/group/contribution_analytics/index.md) **(STARTER)**                                                                                                               | See detailed statistics of group contributors.                                                   |
| [Create](gitlab-basics/create-project.md) and [fork](gitlab-basics/fork-project.md) projects, and<br/>[import and export projects<br/>between instances](user/project/settings/import_export.md) | Create, duplicate, and move projects.                                                            |
| [File locking](user/project/file_lock.md) **(PREMIUM)**                                                                                                                                          | Lock files to avoid merge conflicts.                                                             |
| [GitLab Pages](user/project/pages/index.md)                                                                                                                                                      | Build, test, and deploy your static website with GitLab Pages.                                   |
| [Groups](user/group/index.md) and [Subgroups](user/group/subgroups/index.md)                                                                                                                     | Organize your projects in groups.                                                                |
| [Issue Analytics](user/group/issues_analytics/index.md) **(PREMIUM)**                                                                                                                           | Check how many issues were created per month.                                                    |
| [Merge Request Analytics](user/analytics/merge_request_analytics.md) **(PREMIUM)**                                                                                                                           | Check your throughput productivity - how many merge requests were merged per month.                                                    |
| [Projects](user/project/index.md), including [project access](public_access/public_access.md)<br/>and [settings](user/project/settings/index.md)                                                 | Host source code, and control your project's visibility and set configuration.                   |
| [Search through GitLab](user/search/index.md)                                                                                                                                                    | Search for issues, merge requests, projects, groups, and todos.                                  |
| [Snippets](user/snippets.md)                                                                                                                                                                     | Snippets allow you to create little bits of code.                                                |
| [Web IDE](user/project/web_ide/index.md)                                                                                                                                                         | Edit files within GitLab's user interface.                                                       |
| [Static Site Editor](user/project/static_site_editor/index.md)                                                                                                                                   | Edit content on static websites.                                                                 |
| [Wikis](user/project/wiki/index.md)                                                                                                                                                              | Enhance your repository documentation with built-in wikis.                                       |

<div align="right">
  <a type="button" class="btn btn-default" href="#overview">
    Back to Overview <i class="fa fa-angle-double-up" aria-hidden="true"></i>
  </a>
</div>
